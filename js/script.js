let firstNumber = null;
let secondNumber = null;
let operationFunction = null;
function isNumber(number) {
    return !isNaN(parseInt(number)) && isFinite(number) && number !== null && number !== '';
}

function generateOperationCallback(operation) {
    switch (operation) {
        case '+':
            return function(a, b) {
                return firstNumber + secondNumber;
            }
        case '-':
            return function(a, b) {
                return firstNumber - secondNumber;
            }
        case '*':
            return function(a, b) {
                return firstNumber * secondNumber;
        }
        case '/':
            return function(a, b) {
                return firstNumber / secondNumber;
            }
        default:
            return null;
    }
}


while (true) {
    firstNumber = prompt('Введите первое число');
    secondNumber = prompt('Введите второе число');
    let operation = prompt('Введите операцию');
    operationFunction = generateOperationCallback(operation);

    firstNumber = parseInt(firstNumber);
    secondNumber = parseInt(secondNumber);

    if (isNumber(firstNumber) && isNumber(secondNumber) && operationFunction !== null) {
        break;
    }
}

const result = operationFunction(firstNumber, secondNumber);
console.log(result);